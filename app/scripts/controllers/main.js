'use strict';

/**
 * @ngdoc function
 * @name gridsterDemoApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the gridsterDemoApp
 */
angular.module('gridsterDemoApp').
controller('MainCtrl', function ($scope) {

    $scope.grid = {
        editable: false,

        widgets: [],

        settings:  {
            columns: 6, // the width of the grid, in columns
            pushing: true, // whether to push other items out of the way on move or resize
            floating: true, // whether to automatically float items up so they stack (you can temporarily disable if you are adding unsorted items with ng-repeat)
            swapping: false, // whether or not to have items of the same size switch places instead of pushing down if they are the same size
            width: 'auto', // can be an integer or 'auto'. 'auto' scales gridster to be the full width of its containing element
            colWidth: 'auto', // can be an integer or 'auto'.  'auto' uses the pixel width of the element divided by 'columns'
            rowHeight: 'match', // can be an integer or 'match'.  Match uses the colWidth, giving you square widgets.
            margins: [10, 10], // the pixel distance between each widget
            outerMargin: true, // whether margins apply to outer edges of the grid
            isMobile: false, // stacks the grid items if true
            mobileBreakPoint: 600, // if the screen is not wider that this, remove the grid layout and stack the items
            mobileModeEnabled: true, // whether or not to toggle mobile mode when screen width is less than mobileBreakPoint
            minColumns: 1, // the minimum columns the grid must have
            minRows: 2, // the minimum height of the grid, in rows
            maxRows: 100,
            defaultSizeX: 2, // the default width of a gridster item, if not specified
            defaultSizeY: 1, // the default height of a gridster item, if not specified
            resizable: {
               enabled: false,
               handles: ['e', 's', 'se'],
               // start: function(event, $element, widget) {}, // optional callback fired when resize is started,
               // resize: function(event, $element, widget) {}, // optional callback fired when item is resized,
               // stop: function(event, $element, widget) {} // optional callback fired when item is finished resizing
            },
            draggable: {
               enabled: false, // whether dragging items is supported
               handle: '.handler', // optional selector for resize handle
               // start: function(event, $element, widget) {}, // optional callback fired when drag is started,
               // drag: function(event, $element, widget) {}, // optional callback fired when item is moved,
               // stop: function(event, $element, widget) {} // optional callback fired when item is finished dragging
            }
        },

        openSettings: function(index) {
            $scope.$emit('open-dialog', index);
        },

        addWidget: function() {
            $scope.toggleEditable(true);
            $scope.grid.widgets.push({row: 0, col: 0});
        }
    };

    $scope.toggleEditable = function (editable) {
        $scope.grid.editable = editable;
        $scope.grid.settings.resizable.enabled = editable;
        $scope.grid.settings.draggable.enabled = editable;
    };

});
