'use strict';

/**
 * @ngdoc function
 * @name gridsterDemoApp.controller:DialogCtrl
 * @description
 * # DialogCtrl
 * Controller of the gridsterDemoApp
 */
angular.module('gridsterDemoApp').
controller('DialogCtrl', function ($scope) {
    var dialog = this;

    dialog.show = false;
    dialog.widget = null;

    $scope.$on('open-dialog', function (e, widget) {
        dialog.show = true;
        dialog.widget = widget;
    });
});
