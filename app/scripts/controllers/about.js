'use strict';

/**
 * @ngdoc function
 * @name gridsterDemoApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the gridsterDemoApp
 */
angular.module('gridsterDemoApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
